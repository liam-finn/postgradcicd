using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using SocialEyes.Areas.Identity.Pages;
using SocialEyes.Controllers;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using SocialEyes.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        private readonly MockRepository _Mockery;

        public UnitTest1()
        {
            _Mockery = new MockRepository(MockBehavior.Loose);
        }
        //Check that the Home index method returns a view
        [Fact]
        public void VerifyIndexViewType()
        {
            //Arrange
            var controller = new HomeController();
            //Act
            var result = controller.Index();
            //Assert
            Assert.IsType<ViewResult>(result);
        }

        //Verifying that Image Service is Successfully reading from the dbcontext
        [Fact]
        public void VerifyImageService()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Applcation")
                .Options;

            var context = new ApplicationDbContext(options);

            Seed(context);

            var query = new ImageService(context);
            //Act
            var result = query.GetAll();
            int total = 0;
            foreach(ImageModel t in result)
            {
                total = total+ 1;
            }
            //Assert
            Assert.Equal(2,total);
        }

        //Verifying that Image Service is Successfully reading from the dbcontext
        [Fact]
        public void VerifyGalleryService()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Applcation")
                .Options;

            var context = new ApplicationDbContext(options);

            Seed(context);

            var query = new GallerysService(context);
            //Act
            Task<ICollection<GalleryModel>> result = query.GetAllGallerysForUserAsync("1");

            //Assert
            Assert.NotNull(result);
        }

        //Fill DBContext with 2 new GalleryModels
        private void Seed(ApplicationDbContext context)
        {
            var galleries = new[]
            {
                new GalleryModel {ID=1, Name = "Test1", Location="TestLocation", Duration=2, UserRefID="1",Images = new List<ImageModel> { new ImageModel {Id = 1, Title = "TestTitle" } } },
                new GalleryModel {ID=2, Name = "Test2", Location="TestLocation2", Duration=2 ,UserRefID="2", Images = new List<ImageModel> { new ImageModel {Id = 2, Title = "TestTitle2" } } }
            };

            context.Gallerys.AddRange(galleries);
            context.SaveChanges();
        }

        //Test that the gallery index is returning a list of galleries when userRef is set +
        //also test it returns nothing when userRef is not set or if the user owns no galleries

        //Test the gallery creation successfully creates a gallery + stores data in db

        //Test the Gallery Delete succesfuly deletes the gallery

        //test the settings can be edited for each gallery

        //Test that images can be uploaded to a gallery + stored in blob storage + stored in db

        //Test that 
    }
}
