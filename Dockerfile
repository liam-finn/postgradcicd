# Base image definition
FROM mcr.microsoft.com/dotnet/sdk:2.1 AS build-env
# Copying files to Docker directory
WORKDIR /docker
COPY ["SocialEyes/*.csproj", "./"]
# Restore
RUN dotnet restore
COPY . ./
# Publish
RUN dotnet publish ./SocialEyes/*.csproj -c Release -o out
# Runtime
FROM mcr.microsoft.com/dotnet/aspnet:2.1
WORKDIR /docker
EXPOSE 80
COPY --from=build-env /docker/SocialEyes/out .
ENTRYPOINT ["dotnet", "SocialEyes.dll"]
