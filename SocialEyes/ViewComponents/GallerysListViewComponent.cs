﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using SocialEyes.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialEyes.ViewComponents
{
    public class GallerysListViewComponent : ViewComponent

    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IGallerysService _iGalleryService;

        public GallerysListViewComponent(IHttpContextAccessor httpContext, IGallerysService iGalleryService)
        {
            _httpContext = httpContext;
            _iGalleryService = iGalleryService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userId = _httpContext.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return View(await _iGalleryService.GetAllGallerysForUserAsync(userId));

        }

    }
}
