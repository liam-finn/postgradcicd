﻿using Microsoft.AspNetCore.Mvc;
using SocialEyes.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.ViewComponents
{
    public class CommentsViewComponent : ViewComponent
    {
        public ICommentsService _iCommentsService;

        public CommentsViewComponent(ICommentsService iCommentsService)
        {
            _iCommentsService = iCommentsService;
        }

        public async Task<IViewComponentResult> InvokeAsync(int ImageId)
        {
            var Comments = await _iCommentsService.GetAllCommentsForImage(ImageId);
            return View(Comments);
        }

    }
}
