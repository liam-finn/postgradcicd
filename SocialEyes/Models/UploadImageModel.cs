﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Models
{
    public class UploadImageModel
    {
        public string Title { get; set; }
        public List<IFormFile> ImageUpload { get; set; }
        public int GalleryId { get; set; }

        public UploadImageModel()
        {

        }
    }
}