﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using SocialEyes.Models;
using SocialEyes.Services;



namespace SocialEyes.Controllers
{
    //This Controller handles the method calls relating to the creation, deletion and display of the GalleryModel
    public class GalleryController : Controller
    {
        //**************************************
        //GalleryService handles all Database manipulations for the GalleryModel 
        private readonly IGallerysService _galleryService;
        private IImageService _imageService;
        private IAzureBlobService _azureBlobService;

        //*************************************
        //Injecting all dependencies required by the GalleryController
        public GalleryController(IAzureBlobService azureBlobService, IImageService imageService, IGallerysService galleryService)
        {
            _imageService = imageService;
            _azureBlobService = azureBlobService;
            _galleryService = galleryService;
        }
        //Returns a list of all Galleries for the currently logged in user
        [Authorize]
        // GET: Gallery
        public async Task<IActionResult> Index()
        {
            //************************************************
            //CM 25/11/18
            //Get userID based on the currently logged in user
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier).Value;

            //************************************************
            //CM 25/11/18
            //Send the collection of Gallerys based on the logged in user
            return View(await _galleryService.GetAllGallerysForUserAsync(userId));
        }

        // GET: Gallery/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galleryModel = await _galleryService.GetGalleryAsync(id);

            if (galleryModel == null)
            {
                return NotFound();
            }

            return View(galleryModel);
        }

        //Returns the view that displays all editable settings for the Gallery, Gallery deletion is also handled in 
        //the resulting view here
        public async Task<IActionResult> Settings(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galleryModel = await _galleryService.GetGalleryAsync(id);

            if (galleryModel == null)
            {
                return NotFound();
            }

            return View(galleryModel);
        }

        //Creates a new GalleryModel Object based on the currently logged in user
        // GET: Gallery/Create
        public IActionResult Create()
        {
            ViewData["UserRefID"] = User.FindFirst(ClaimTypes.NameIdentifier).Value; 
            return View(new GalleryModel() {StartDateTime = DateTime.Now });
        }

        //Following the gallery create view this method takes the user input and generates a new gallery 
        // POST: Gallery/Create
        // HTTP Post attribute indicates that 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Name,Location,Duration,StartDateTime,AllowUsersAddImages,UserRefID")] GalleryModel galleryModel)
        {
            if (ModelState.IsValid)
            {
                galleryModel.UniqueUrl = Guid.NewGuid().ToString().Substring(0,6);
                galleryModel.DateCreated = DateTime.Now;
                await _galleryService.CreateGallery(galleryModel);
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserRefID"] = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return View();
        }

        //Returns the gallery open view, this view is available regardless of current userRefId
        [ActionName("Open")]
        // GET: Gallery/Open/5
        public async Task<IActionResult> OpenWithCode(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var galleryModel = await _galleryService.GetGalleryAsync(id);

            return OpenGallery(galleryModel);
        }

        // GET: Gallery/Open/5
        [ActionName("OpenWithId")]
        public async Task<IActionResult> OpenWithId(int id)
        {

            var galleryModel = await _galleryService.GetGalleryAsync(id);
            return OpenGallery(galleryModel);

        }

        // GET: Gallery/Open/5

        public IActionResult OpenGallery(GalleryModel galleryModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (galleryModel.UserRefID == User.FindFirst(ClaimTypes.NameIdentifier).Value)
                {
                    ViewBag.IsOwner = true;
                    return View("Open",galleryModel);
                }
            }
            ViewBag.IsOwner = false;
            if (HasEventStarted(galleryModel.StartDateTime))
            {
                if (IsEventLive(galleryModel.StartDateTime, galleryModel.Duration))
                {
                    return View("Open", galleryModel);
                }
                ViewBag.GalleryName = galleryModel.Name;
                return View("Finished");
            }
            else //Redirect to sorry this gallery has finished
            {
               
                return View("NotStarted");
            }
        }

        //Delete asingle image from a gallery
        //This removes the image reference from the gallery model so it is useful to have in the gallery controller
        public async Task<IActionResult> ImageDelete(int? id)
        {
            var galleryModel = await _galleryService.GetGalleryAsync(id);
            return View(galleryModel);
        }

        //Sets the main image to be used as a thumbnail
        public async Task<IActionResult> SetMainImage(int GalleryID, string UrlMedium)
        {
            
            await _imageService.SetMainImage(UrlMedium, GalleryID);

            return RedirectToAction(nameof(Index));
        }

        // POST: Gallery/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,UserRefID,UniqueUrl,Name,Location,Duration,StartDateTime,AllowUsersAddImages,MainImageUrl")] GalleryModel galleryModel)
        {
            if (id != galleryModel.ID)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await _galleryService.UpdateGallery(galleryModel);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!(await _galleryService.CheckIfGalleryExists(galleryModel.ID)))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserRefID"] = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return View(galleryModel);
        }

        // GET: Gallery/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var galleryModel = await _galleryService.GetGalleryAsync(id); 

            if (galleryModel == null)
            {
                return NotFound();
            }

            return View(galleryModel);
        }

        // POST: Gallery/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var galleryModel = await _galleryService.GetGalleryAsync(id);
            await _galleryService.DeleteGallery(galleryModel);
            return RedirectToAction(nameof(Index));
        }


        public IActionResult Upload(int id)
        {
            var model = new UploadImageModel
            {
                GalleryId = id
            };
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> UploadNewImage(List<IFormFile> file, string title, int GalleryId)
        {
            var galleryModel = await _galleryService.GetGalleryAsync(GalleryId); 
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            foreach (IFormFile f in file)
            {
                Byte[] resizedImageMedium;
                using (var memoryStream = new MemoryStream())
                {
                    await f.CopyToAsync(memoryStream);
                    resizedImageMedium = await _imageService.ResizeImage(memoryStream);
                }
                var blobUri = await _azureBlobService.UploadToBlobContainer(f.OpenReadStream(), f.FileName);

                using (var stream = new MemoryStream(resizedImageMedium))
                {
                    var blobUriMedium = await _azureBlobService.UploadToBlobContainer(stream, _imageService.RenameImage(f.FileName, "_md"));
                    if (galleryModel.Images.Count == 0)
                    {
                        await _imageService.SetMainImage(blobUriMedium.ToString(),GalleryId);
                    }
                    await _imageService.SetImage(title, blobUri, blobUriMedium, GalleryId, userId);
                }

            }
            return RedirectToAction("OpenWithID/" + galleryModel.ID, "Gallery");
        }

        //*********************************
        //Confirms that event has started
        private bool HasEventStarted(DateTime? startDateTime)
        {
            DateTime startDate = startDateTime ?? DateTime.Now;
            int dateCompare = DateTime.Compare(startDate, DateTime.Now);
            ViewBag.GalleryStartTime = startDate.ToLocalTime();
            //If value is greater than or equal to zero the gallery has started
            if ( dateCompare < 0  )
            {
                
                return true;
            }
            return false;
        }

        //**********************************
        //Checks to see if the event is live
        private bool IsEventLive(DateTime? startDateTime, int duration)
        {
            DateTime startDate = startDateTime ?? DateTime.Now;
            DateTime endDate = startDate.AddHours(duration);
            ViewBag.GalleryEndTime = endDate.ToLocalTime();
            //If value is less than 0 the event is live
            if (DateTime.Compare(endDate, DateTime.Now) > 0  )
            {
               
                
                return true;
            }
            return false;
        }
    }
}
