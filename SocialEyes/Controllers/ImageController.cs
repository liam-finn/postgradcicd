﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using Microsoft.AspNetCore.Http.Extensions;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace SocialEyes.Controllers
{
    public class ImageController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ImageController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Open(int id)
        {
            var imageModel = _context.Images.FirstOrDefault(m => m.Id == id);
            return View(imageModel);
        }

        public IActionResult Delete(int id)
        {
            var imageModel = _context.Images.FirstOrDefault(m => m.Id == id);


            if (imageModel == null)
            {
                return NotFound();
            }

            return View(imageModel);
        }

        // POST: Gallery/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var imageModel = await _context.Images.FindAsync(id);
            int GalleryId = imageModel.GalleryID;
            _context.Images.Remove(imageModel);
            await _context.SaveChangesAsync();
            //return RedirectToAction("OpenWithId/" + GalleryId,"Gallery");
            return RedirectToAction("Index", "Gallery");
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateCommentAsync([Bind("CommentValue,ImageId")]CommentModel cm)
        {
            if (ModelState.IsValid)
            {
                cm.UserId= User.FindFirst(ClaimTypes.NameIdentifier).Value;
                cm.DateCreated = DateTime.Now;
                _context.Add(cm);
                await _context.SaveChangesAsync();
                
            }
            return RedirectToAction("Open/" + cm.ImageId);
        }
    }
}