﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialEyes.Data;
using SocialEyes.Models;

namespace SocialEyes.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult JoinGallery(string Code)
        {
            var galleryModel = _context.Gallerys.FirstOrDefault(u => u.UniqueUrl == Code);
            if (galleryModel == null)
            {
                return NotFound();
            }

            return RedirectToAction("Open/" + Code, "Gallery");
        }
    }
}
