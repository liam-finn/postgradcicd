﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SocialEyes.Data.DBModels;
using SocialEyes.Models;

namespace SocialEyes.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GalleryModel>()
                .HasOne(g => g.ApplicationUser)
                .WithMany(au => au.Gallerys);
            modelBuilder.Entity<GalleryModel>()
                .HasMany(g => g.Images);

            modelBuilder.Entity<CommentModel>()
                .HasOne(c => c.ApplicationUser);

            modelBuilder.Entity<CommentModel>()
                .HasOne(c => c.ImageModel)
                .WithMany(i => i.Comments);
        }

        
        public DbSet<GalleryModel> Gallerys { get; set; }
        public DbSet<ImageModel> Images { get; set; }
        public DbSet<CommentModel> Comments { get; set; }

    }
}
