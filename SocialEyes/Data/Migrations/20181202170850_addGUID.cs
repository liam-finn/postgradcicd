﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialEyes.Data.Migrations
{
    public partial class addGUID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UniqueUrl",
                table: "Gallery",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UniqueUrl",
                table: "Gallery");
        }
    }
}
