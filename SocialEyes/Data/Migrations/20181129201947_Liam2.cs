﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialEyes.Data.Migrations
{
    public partial class Liam2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Gallery_ImageID",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ImageID",
                table: "Gallery");

            migrationBuilder.RenameColumn(
                name: "ImageID",
                table: "Images",
                newName: "GalleryModelID");

            migrationBuilder.RenameIndex(
                name: "IX_Images_ImageID",
                table: "Images",
                newName: "IX_Images_GalleryModelID");

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Gallery_GalleryModelID",
                table: "Images",
                column: "GalleryModelID",
                principalTable: "Gallery",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Gallery_GalleryModelID",
                table: "Images");

            migrationBuilder.RenameColumn(
                name: "GalleryModelID",
                table: "Images",
                newName: "ImageID");

            migrationBuilder.RenameIndex(
                name: "IX_Images_GalleryModelID",
                table: "Images",
                newName: "IX_Images_ImageID");

            migrationBuilder.AddColumn<int>(
                name: "ImageID",
                table: "Gallery",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Gallery_ImageID",
                table: "Images",
                column: "ImageID",
                principalTable: "Gallery",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
