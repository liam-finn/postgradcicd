﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialEyes.Data.Migrations
{
    public partial class Liam1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EventID",
                table: "Gallery",
                newName: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Gallery",
                newName: "EventID");
        }
    }
}
