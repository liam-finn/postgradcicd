﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialEyes.Data.Migrations
{
    public partial class Test2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Gallery_GalleryModelID",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Images_GalleryModelID",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "GalleryModelID",
                table: "Images");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GalleryModelID",
                table: "Images",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Images_GalleryModelID",
                table: "Images",
                column: "GalleryModelID");

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Gallery_GalleryModelID",
                table: "Images",
                column: "GalleryModelID",
                principalTable: "Gallery",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
