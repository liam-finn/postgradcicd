﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Data.DBModels
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<GalleryModel> Gallerys { get; set; }

    }
}
