﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Data.DBModels
{
    [Table("Comment")]
    public class CommentModel
    {
        [Key]
        public int Id { get; set; }
        public string CommentValue { get; set; }
        public DateTime DateCreated { get; set; }
        public int ImageId { get; set; }
        public string UserId { get; set; }

        [ForeignKey("ImageId")]
        public ImageModel ImageModel { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }



  
    }
}
