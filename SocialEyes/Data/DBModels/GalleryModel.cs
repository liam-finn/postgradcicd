﻿using Microsoft.AspNetCore.Identity;
using SocialEyes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Data.DBModels
{
    [Table("Gallery")]
    public class GalleryModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }

        public int Duration { get; set; }
        public string UniqueUrl { get; set; }
        public string MainImageUrl { get; set; }

        
        public DateTime DateCreated { get; set; }
        
        public DateTime? StartDateTime { get; set; }

        public Boolean AllowUsersAddImages { get; set; } = false;

        public string UserRefID { get; set; }
        [ForeignKey("UserRefID")]
        public ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<ImageModel> Images { get; set; }
    }

}
