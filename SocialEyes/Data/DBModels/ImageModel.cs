﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace SocialEyes.Data.DBModels
{
    public class ImageModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public string Url { get; set; }
        public string UrlMedium { get; set; }

        public int GalleryID { get; set; }
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("GalleryID")]
        public virtual GalleryModel GalleryModel { get; set; }

        public  ICollection<CommentModel> Comments { get; set; }
    }
}
