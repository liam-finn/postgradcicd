﻿using Microsoft.EntityFrameworkCore;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
//using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using SocialEyes.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    public class ImageService : IImageService
    {
        private readonly ApplicationDbContext _ctx;

        public ImageService(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }
        public IEnumerable<ImageModel> GetAll()
        {
            return _ctx.Images;
        }

        public ImageModel GetById(int id)
        {
            return _ctx.Images.Find(id);
        }

        public async Task SetImage(string title, Uri uri, Uri uriMedium, int GalleryID,string UserId)
        {
            var image = new ImageModel
            {
                Title = title,
                Url = uri.AbsoluteUri,
                UrlMedium = uriMedium.AbsoluteUri,
                Created = DateTime.Now,
                GalleryID = GalleryID,
                UserId = UserId
            };

            _ctx.Add(image);
            await _ctx.SaveChangesAsync();
        }

        public async Task SetMainImage(string uri, int GalleryId)
        {
            GalleryModel gallery = await _ctx.Gallerys.Where(m => m.ID.Equals(GalleryId)).FirstOrDefaultAsync();
            _ctx.Update(gallery);
            gallery.MainImageUrl = uri.ToString();

            _ctx.SaveChanges();
            await _ctx.SaveChangesAsync();
        }

        //***************************************************************
        //CM 24/11/18
        //Resize an image to any resolution. takes in  appending a string value to the end of the filename
        //Appends the value before the file extension
        public async Task<byte[]> ResizeImage(MemoryStream memoryStream, int width = 50, int height = 50)
        {

            using (memoryStream)
            {

                using (var image = Image.Load(memoryStream.ToArray(), out IImageFormat format))
                {
                    image.Mutate(i => i.Resize(width, height));
                    using (var stream2 = new MemoryStream())
                    {
                        image.Save(stream2, format);
                        return stream2.ToArray();
                    }

                }
            }
        }

        //***************************************************************
        //CM 20/11/18
        //Rename files by appending a string value to the end of the filename
        //Appends the value before the file extension
        public string RenameImage(string filename, string stringToAppend)
        {
            var fileName = Path.GetFileNameWithoutExtension(filename);
            var extension = Path.GetExtension(filename);
                        
            var fileNameResized = fileName + stringToAppend + extension;
            return fileNameResized;
        }
    }
}
