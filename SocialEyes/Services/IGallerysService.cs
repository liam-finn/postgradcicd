﻿using SocialEyes.Data.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    public interface IGallerysService
    {
        Task<ICollection<GalleryModel>> GetAllGallerysForUserAsync(string userId);
        Task<GalleryModel> GetGalleryAsync(int? galleryId);
        Task<GalleryModel> GetGalleryAsync(string galleryUnique);

        Task<bool> CreateGallery(GalleryModel galleryModel);

        Task<bool> DeleteGallery(GalleryModel galleryModel);
        Task<int> UpdateGallery(GalleryModel galleryModel);

        Task<bool> CheckIfGalleryExists(int id);

    }
}
