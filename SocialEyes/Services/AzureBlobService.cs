﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    public class AzureBlobService : IAzureBlobService
    {
        private string azureConnectionString;
        private string containerName;

        //Takes the connection strings from the configuration file
        public AzureBlobService(IConfiguration config)
        {
            azureConnectionString = config.GetConnectionString("BlobConnection");
            containerName = config.GetConnectionString("BlobContainerName");
        }
        
        //Connects the applcation to the Azure Blob storage by getting the ContainerClientsID
        private CloudBlobContainer GetBlobContainer()
        {
            var storageAccount = CloudStorageAccount.Parse(azureConnectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(containerName);
        }

        //26/11/18
        //Uploads to a Blob container and returns the resources new URI
        public async Task<Uri> UploadToBlobContainer(Stream st, string filename)
        {
            var container = GetBlobContainer();
            var blockBlob = container.GetBlockBlobReference(filename);
            await blockBlob.UploadFromStreamAsync(st);
            return blockBlob.Uri;
        }
    }
}
