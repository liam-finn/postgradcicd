﻿using Microsoft.EntityFrameworkCore;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    public class CommentsService :ICommentsService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public CommentsService (ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<ICollection<CommentModel>> GetAllCommentsForImage(int id)
        {
            var imageModel = await _applicationDbContext.Images
                .Where(i => i.Id == id)
                .Include(i => i.Comments)
                .FirstOrDefaultAsync();

            return imageModel.Comments;
        }
    }
}
