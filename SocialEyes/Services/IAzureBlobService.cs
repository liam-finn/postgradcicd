﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    //Interface to base the AzureBlobService on
    public interface IAzureBlobService
    {
        Task<Uri> UploadToBlobContainer(Stream st, string filename);
    }
}