﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SocialEyes.Data.DBModels;

namespace SocialEyes.Services
{
    public interface IImageService
    {
        IEnumerable<ImageModel> GetAll();
        ImageModel GetById(int id);
        string RenameImage(string filename, string stringToAppend);
        Task<byte[]> ResizeImage(MemoryStream memoryStream, int width = 50, int height = 50);
        Task SetImage(string title, Uri uri, Uri uriMedium, int GalleryID, string UserId);
        Task SetMainImage(string uri, int GalleryId);
    }
}