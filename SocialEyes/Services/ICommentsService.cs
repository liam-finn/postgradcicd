﻿using SocialEyes.Data.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialEyes.Services
{
    public interface ICommentsService
    {
        Task<ICollection<CommentModel>> GetAllCommentsForImage(int id);

    }
}
