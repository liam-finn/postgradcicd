﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialEyes.Data;
using SocialEyes.Data.DBModels;

namespace SocialEyes.Services
{
    public class GallerysService : IGallerysService
    {
        private readonly ApplicationDbContext _context;
        public GallerysService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GalleryModel> GetGalleryAsync(int? galleryId)
        {
            var galleryModel = await _context.Gallerys
               .Include(g => g.ApplicationUser)
               .Include(g => g.Images)
               .FirstOrDefaultAsync(m => m.ID == galleryId);

            return galleryModel;
        }
        public async Task<GalleryModel> GetGalleryAsync(string galleryUnique)
        {
            var galleryModel = await _context.Gallerys
               .Include(g => g.ApplicationUser)
               .Include(g => g.Images)
               .FirstOrDefaultAsync(m => m.UniqueUrl == galleryUnique);

            return galleryModel;
        }

        public async Task<ICollection<GalleryModel>> GetAllGallerysForUserAsync(string userId)
        {
            //************************************************
            //CM 25/11/18
            //Return an ApplicationUser object with list of Gallerys
            var userModel = await _context.Users
                .Where(u => u.Id == userId)
                .Include(u => u.Gallerys)
                .FirstOrDefaultAsync();

            return userModel.Gallerys;

        }
        public async Task<bool> DeleteGallery(GalleryModel galleryModel)
        {
            using (_context)
            {
                _context.Gallerys.Remove(galleryModel);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> CreateGallery(GalleryModel galleryModel)
        {
            using (_context)
            {
                _context.Add(galleryModel);
                await _context.SaveChangesAsync();
                return true;
            }
        }
        public async Task<int> UpdateGallery(GalleryModel galleryModel)
        {
            using (_context)
            {
                _context.Update(galleryModel);
                return await _context.SaveChangesAsync();
            }
        }

        public async Task<bool> CheckIfGalleryExists(int id)
        {
            return await _context.Gallerys.AnyAsync(e => e.ID == id);
        }
    }
}
